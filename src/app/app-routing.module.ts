import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './library/shared/components/home/home.component';
import { AuthGuard } from './auth.guard';


const routes: Routes = [


  { path: '', component: HomeComponent },
  { path: 'company', loadChildren: "./library/auth/auth.module#AuthModule" },
  { path: 'pages', loadChildren: './areas/global-pages/global-pages.module#GlobalPagesModule', },
  { path: '', loadChildren: "./areas/user/user.module#UserModule", canActivate: [AuthGuard] },
  { path: '', loadChildren: "./areas/handle-product/handle-product.module#HandleProductModule", canActivate: [AuthGuard] },
  { path: '', loadChildren: "./areas/blogapp/blogapp.module#BlogappModule", canActivate: [AuthGuard] },
  { path: '', loadChildren: "./areas/layout/layout.module#LayoutModule", canActivate: [AuthGuard], },
  {
    path: 'user',
    loadChildren: "./areas/user/user.module#UserModule",
    canActivate: [AuthGuard],
  },

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: [

  ]
})
export class AppRoutingModule { }
