import { GlobalPagesRoutingModule } from './global-pages-routing.module';

describe('GlobalPagesRoutingModule', () => {
  let globalPagesRoutingModule: GlobalPagesRoutingModule;

  beforeEach(() => {
    globalPagesRoutingModule = new GlobalPagesRoutingModule();
  });

  it('should create an instance', () => {
    expect(globalPagesRoutingModule).toBeTruthy();
  });
});
