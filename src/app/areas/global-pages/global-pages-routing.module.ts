import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, } from '@angular/router';


const routes: Routes=[
  {path:'',loadChildren:'./services/services.module#ServicesModule'}

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: []
})
export class GlobalPagesRoutingModule { }
