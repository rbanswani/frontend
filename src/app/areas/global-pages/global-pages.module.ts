import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalPagesRoutingModule } from './global-pages-routing.module';

@NgModule({
  imports: [
    CommonModule,
    GlobalPagesRoutingModule
  ],
  declarations: []
})
export class GlobalPagesModule { }
