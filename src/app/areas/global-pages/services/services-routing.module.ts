import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq/faq.component';
import { Routes, RouterModule } from '@angular/router';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { PartnerComponent } from './partner/partner.component';


const routes: Routes=[
  {path:'faq',component:FaqComponent},
  {path:'privacypolicy',component:PrivacypolicyComponent},
  {path:'partner',component:PartnerComponent},


]


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class ServicesRoutingModule { }
