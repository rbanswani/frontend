import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq/faq.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { PartnerComponent } from './partner/partner.component';
import { ServicesRoutingModule } from './services-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ServicesRoutingModule
  ],
  declarations: [FaqComponent, PrivacypolicyComponent, PartnerComponent]
})
export class ServicesModule {

}
