import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRouter } from './user.route';

import { DashboardComponent } from './Modules/dashboard/dashboard.component';
import { TopnavComponent } from './Modules/components/topnav/topnav.component';
import { SidebarComponent } from './Modules/components/sidebar/sidebar.component';
import { AdduserComponent } from './admindata/adduser/adduser.component';
import { ListuserComponent } from './admindata/listuser/listuser.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EdituserComponent } from './admindata/edituser/edituser.component';
import { UserCComponent } from './user-c/user-c.component';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {ScheduleModule} from 'primeng/schedule';
@NgModule({
  imports: [
    CommonModule,
    ConfirmDialogModule,
    MessagesModule,
    MessageModule,
    ReactiveFormsModule, FormsModule,
    UsersRouter,
    ScheduleModule
  ],

  declarations: [DashboardComponent, TopnavComponent, SidebarComponent, AdduserComponent,
    ListuserComponent, AdduserComponent, ListuserComponent, EdituserComponent,
     UserCComponent],
     providers:[ConfirmationService]
})


export class UserModule { }
