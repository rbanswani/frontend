import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './Modules/dashboard/dashboard.component';
import { AdduserComponent } from './admindata/adduser/adduser.component';
import { ListuserComponent } from './admindata/listuser/listuser.component';
import { UserCComponent } from './user-c/user-c.component';
import { AuthGuard } from '../../auth.guard';


const routes: Routes = [

  {
    path: '',
    component: UserCComponent,
    canActivate: [AuthGuard],
    children: [{
      path: 'dashboard',
      component: DashboardComponent
    }, {
      path: 'add-user',
      component: AdduserComponent,
    },
    {
      path: 'userlist',
      component: ListuserComponent
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    },
    {
      path: 'product',
      loadChildren: '../handle-product/handle-product.module#HandleProductModule'
    },
    {
      path: 'adblog',
      loadChildren: '../blogapp/blogapp.module#BlogappModule'
    }
    ]
  }


]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class UsersRouter { }