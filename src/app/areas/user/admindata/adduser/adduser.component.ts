import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ServicesModule } from 'src/app/services/services.module';
import { Router } from '@angular/router';


@Component({
    selector: 'app-adduser',
    templateUrl: './adduser.component.html',
    styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

    registerForm: FormGroup;
    submitted = false;

    constructor(private formBuilder: FormBuilder,private add: ServicesModule , private router: Router) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            // lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    addUser() { 
    this.submitted = true;
    console.log(this.registerForm.value);
    this.add.adduser(this.registerForm.value)
        .subscribe((res: any) => {
            res = res.data;
            console.log("data", res)  
        })

        if (this.registerForm.invalid) {
            return;
        }
            else{
                // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
                this.router.navigate(['/userlist']);
            }
    }

}







