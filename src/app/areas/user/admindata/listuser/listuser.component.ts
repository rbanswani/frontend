import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { HttpClient } from '@angular/common/http';
import { ConfirmationService, Message } from 'primeng/api';


@Component({
  selector: 'app-listuser',
  templateUrl: './listuser.component.html',
  styleUrls: ['./listuser.component.css']
})
export class ListuserComponent implements OnInit {

  listArr: any


  constructor(public httpClient: HttpClient,
    private confirmationService: ConfirmationService,
    private listserv: UserService) { }

  ngOnInit() {
    this.getListUser();
  }
  getListUser() {
    this.listserv.listUser().subscribe(res => {
      console.log("ress=========");
      this.listArr = res;
    });
  }
  deleteUser(id) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {

        this.listserv.deleteUsers(id).subscribe(res => {
          console.log('confirm')
          console.log("rs", res);
          this.getListUser();

        })
        //Actual logic to perform a confirmation
      },
      reject: () => {
        this.getListUser()
      }
    });

  }
}