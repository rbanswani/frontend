import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { DropdownModule } from 'primeng/dropdown';
import { HandleProductRoutingModule } from './handle-product-routing.module';
import { AddproductComponent } from './pcomponents/addproduct/addproduct.component';
import { ListproductComponent } from './pcomponents/listproduct/listproduct.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditproductComponent } from './pcomponents/editproduct/editproduct.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import {SplitButtonModule} from 'primeng/splitbutton';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HandleProductRoutingModule,
    ConfirmDialogModule,
    MessagesModule,
    MessageModule,
    SplitButtonModule


  ],
  declarations: [AddproductComponent, ListproductComponent, EditproductComponent],
  providers: [ConfirmationService]
})
export class HandleProductModule { }
