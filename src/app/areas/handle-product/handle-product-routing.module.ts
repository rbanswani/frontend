import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddproductComponent } from './pcomponents/addproduct/addproduct.component';
import { ListproductComponent } from './pcomponents/listproduct/listproduct.component';
import { EditproductComponent } from './pcomponents/editproduct/editproduct.component';

const routes: Routes = [

  { path: 'addproduct', component: AddproductComponent },
  { path: 'listproduct', component: ListproductComponent },
  { path: 'editproduct/:id', component: EditproductComponent },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HandleProductRoutingModule { }
