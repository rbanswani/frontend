import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductService } from '../../../../services/product.service';

import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService, Message } from 'primeng/api';
@Component({
  selector: 'app-listproduct',
  templateUrl: './listproduct.component.html',
  styleUrls: ['./listproduct.component.css'],
  providers: [ConfirmationService]
})
export class ListproductComponent implements OnInit {
  listArr: any;

  msgs: Message[] = [];
  constructor(public httpClient: HttpClient, private router: Router,
    private confirmationService: ConfirmationService,
    private pro: ProductService) { }

  ngOnInit() {
    this.getlistProduct();

  }


  getlistProduct() {
    this.pro.listproduct().subscribe(res => {
      console.log("check the list");
      this.listArr = res;
    });
  }

  Deletep(id) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete?',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {

        this.pro.deleteProduct(id).subscribe(res => {
          console.log('confirm')
          console.log("rs", res);
          this.getlistProduct();

        })
        //Actual logic to perform a confirmation
      },
      reject: () => {
        this.getlistProduct()
      }
    });
  }

  Editp(id, data) {
    console.log(' EDIT ===', id, data);


    this.router.navigate(['/editproduct/' + id]);

  }


}

