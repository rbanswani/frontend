import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {

  productForm: FormGroup;
  submitted = false;

  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(private formBuilder: FormBuilder, private ps: ProductService, private router: Router) {

  }

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      productName: ['', Validators.required],
      productCost: ['', Validators.required], 
      status: ['', Validators.required],
      brandName: ['', Validators.required], 
      file: null,
      // phoneNumber: ['', Validators.required],
      // customerName: ['', Validators.required],
      // shippingCost: ['', Validators.required],
      // shippingAddress: ['', Validators.required],
    });
  }
  get f() { return this.productForm.controls; }

  // addProduct() { 
  // this.submitted = true;
  // console.log(this.productForm.value);
  // this.ps.addProduct(this.productForm.value)
  //     .subscribe((res: any) => {
  //         res = res.data;
  //         console.log("data", res)
  //     });
  //   }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.productForm.get('file').setValue(file);
    }
  }

  private prepareSave(): any {
    let input = new FormData();
    input.append('productName', this.productForm.get('productName').value);
    input.append('brandName', this.productForm.get('brandName').value);
    input.append('productCost', this.productForm.get('productCost').value);
    // input.append('shippingCost', this.productForm.get('shippingCost').value);
    // input.append('shippingAddress', this.productForm.get('shippingAddress').value);
    // input.append('phoneNumber', this.productForm.get('phoneNumber').value);
    input.append('status', this.productForm.get('status').value);
    input.append('file', this.productForm.get('file').value);
    return input;
  }

  addProduct() {
    const formModel = this.prepareSave();
    this.ps.addproduct(formModel).subscribe(res => {
      console.log(res)
      console.log("successs");
    })
    if (this.productForm.invalid) {
      return;
    }
    else {}
      this.router.navigate(['/product/listproduct']);
    

  }

  clearFile() {
    this.productForm.get('file').setValue(null);
    this.fileInput.nativeElement.value = '';
  }

}
