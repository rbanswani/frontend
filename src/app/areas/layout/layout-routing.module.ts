import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UserboardComponent } from './others/userboard/userboard.component';

const routes: Routes=[

  {path:'userboard', component:UserboardComponent}
  
]

@NgModule({
  imports: [
    CommonModule,

    RouterModule.forChild(routes)
  ],
  declarations: []
})
export class LayoutRoutingModule { }
