import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserboardComponent } from 'src/app/areas/layout/others/userboard/userboard.component';
import { LayoutRoutingModule } from 'src/app/areas/layout/layout-routing.module';
import {CardModule} from 'primeng/card';
import { SideviewComponent } from './others/components/sideview/sideview.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    CardModule,
   
  ],
  declarations: [UserboardComponent, SideviewComponent]
})
export class LayoutModule { }
