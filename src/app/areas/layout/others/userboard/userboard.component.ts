import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../../services/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userboard',
  templateUrl: './userboard.component.html',
  styleUrls: ['./userboard.component.css']
})
export class UserboardComponent implements OnInit {
  listArr: any;

  constructor( private pro:ProductService,
    private router:Router) { }

  ngOnInit() {
    this.getlistProduct();  
  }

  getlistProduct(){
    this.pro.listproduct().subscribe(res => {
  console.log("check the list");
  this.listArr=res;
   });
  }
  signOut(){
    localStorage.removeItem("token");
    localStorage.removeItem("role");
    this.router.navigate(["/company/login"])
  }

}
