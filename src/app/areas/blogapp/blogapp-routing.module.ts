import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddblogComponent } from './bcomponents/addblog/addblog.component';
import { ListblogComponent } from './bcomponents/listblog/listblog.component';
import { EditblogComponent } from './bcomponents/editblog/editblog.component';



const routes: Routes = [ {path:'addBlog',component:AddblogComponent},
{path:'listBlog',component:ListblogComponent},
{path:'editBlog/:id',component:EditblogComponent},
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogappRoutingModule { }
