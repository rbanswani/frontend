import { BlogappModule } from './blogapp.module';

describe('BlogappModule', () => {
  let blogappModule: BlogappModule;

  beforeEach(() => {
    blogappModule = new BlogappModule();
  });

  it('should create an instance', () => {
    expect(blogappModule).toBeTruthy();
  });
});
