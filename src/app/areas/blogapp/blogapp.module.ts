import { NgModule } from '@angular/core';
 import { CommonModule } from '@angular/common';

import { BlogappRoutingModule } from './blogapp-routing.module';
import { AddblogComponent } from './bcomponents/addblog/addblog.component';
import { ListblogComponent } from './bcomponents/listblog/listblog.component';
import { EditblogComponent } from './bcomponents/editblog/editblog.component';
import { BlogviewComponent } from './blogview/blogview.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {AccordionModule} from 'primeng/accordion';  
import {EditorModule} from 'primeng/editor';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';


@NgModule({
  imports: [
    CommonModule,
    BlogappRoutingModule,
     FormsModule,
     AccordionModule,
     EditorModule,
     ConfirmDialogModule,
    MessagesModule,
    MessageModule,
    

     ReactiveFormsModule
  ],

  declarations: [AddblogComponent, ListblogComponent, EditblogComponent, BlogviewComponent],
  providers:[ConfirmationService]
})
export class BlogappModule { }
