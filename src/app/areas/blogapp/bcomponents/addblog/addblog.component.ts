import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BloggerService } from '../../../../services/blogger.service';
import { ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-addblog',
  templateUrl: './addblog.component.html',
  styleUrls: ['./addblog.component.css']
})
export class AddblogComponent implements OnInit {
  form: FormGroup;
  loading: boolean = false;

  @ViewChild('fileInput') fileInput: ElementRef;


  constructor(public httpClient: HttpClient,
    private fb: FormBuilder, 
    private router: Router,
    private bls: BloggerService) {
    this.blogForm();
  }
  blogForm() {
    this.form = this.fb.group({
      blogtitle: ['', Validators.required],
      blogcontent: ['', Validators.required],
      avatar: null
    });
  }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.form.get('avatar').setValue(file);
    }
  }
  private prepareSave(): any {
    let input = new FormData();
    input.append('blogtitle', this.form.get('blogtitle').value);
    input.append('blogcontent', this.form.get('blogcontent').value);
    input.append('avatar', this.form.get('avatar').value);
    return input;
  }

  onSubmit() {
    const formModel = this.prepareSave();
    this.bls.addBlog(formModel).subscribe(res => {
      console.log(res)
      console.log("successs")
      this.router.navigate(['/listBlog']);
    })


  }

  clearFile() {
    this.form.get('avatar').setValue(null);
    this.fileInput.nativeElement.value = '';
  }

  ngOnInit() {
  }
}
