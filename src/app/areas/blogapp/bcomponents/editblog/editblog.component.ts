import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { BloggerService } from '../../../../services/blogger.service';

@Component({
  selector: 'app-editblog',
  templateUrl: './editblog.component.html',
  styleUrls: ['./editblog.component.css']
})
export class EditblogComponent implements OnInit {
  blogform: FormGroup;


  userData: any;
  route: any;

  constructor(private router:Router,private fb: FormBuilder, private blogserv:BloggerService) { }

  ngOnInit() {
  this.blogform = this.fb.group ({
    blogtitle: new FormControl(''),
    blogcontent: new FormControl(''),
    imagepath: null
  });
// this.getformdata();
  }
 onFileChange(event) {
   if(event.target.files.length > 0) {
     let step =  event.target.files[0];
     this.blogform.get('imagepath').setValue(step); 
   }
 
  }
private prepareSave(): any {
   let input = new FormData();
   input.append('blogtitle', this.blogform.get('blogtitle').value);
   input.append('blogcontent', this.blogform.get('blogcontent').value);
   input.append('imagepath', this.blogform.get('imagepath').value);
    return input;
} 

onUpdate() {
  const BlogModel=this.prepareSave();
  this.route.params.subscribe(params => {
    console.log(" on update ",BlogModel,params['id'])
  
    this.blogserv.EditBlog(BlogModel,params['id']).subscribe(res =>  {
      console.log("successfully edited");
    })
  });
  this.router.navigate(['listBlog'])


}
}

// }
// getformdata(){
//   this.route.params.subscribe(params =>{
//     this.blogserv.listBlog( params['id']).subscribe(res => {
//       console.log("get");
//       console.log(res);
//       this.userData = res;
//       this.blogform.patchValue({
//         blogtitle:this.userData.blogtitle,
//         blogcontent:this.userData.blogcontent,
//         imagepath:this.userData.imagepath
//       })
//     })
//   })
// }



