import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService} from 'primeng/api';
import { BloggerService } from '../../../../services/blogger.service';

@Component({
  selector: 'app-listblog',
  templateUrl: './listblog.component.html',
  styleUrls: ['./listblog.component.css'],
  providers: [ConfirmationService]
})
export class ListblogComponent implements OnInit {
  listArr: any;

    

  constructor(private router:Router,private confirmationService: ConfirmationService,public httpClient: HttpClient,private bro:BloggerService) { }

  ngOnInit() {
    this.getlistblog();
  }

    getlistblog(){
      this.bro.listBlog('id').subscribe(res => {
    console.log("check the list")
    this.listArr=res;
     });
  
    }
    Deleteblog(id) {
      this.confirmationService.confirm({
        message: 'Are you sure that you want to delete?',
        header: 'Delete Confirmation',
         icon: 'pi pi-info-circle',
        accept: () => {
     
        this.bro.deleteBlog(id).subscribe(res =>{
          console.log('confirm')
         console.log("rs",res);
          this.getlistblog();
  
          })
            //Actual logic to perform a confirmation
        },
        reject: () => {
                     this.getlistblog()
                  }
    });
  }

    
    Editblog(id,data){
      console.log(' EDIT ===',id,data);

      this.router.navigate(['/editBlog/'+id]);

    }
   
  }

  




  
  