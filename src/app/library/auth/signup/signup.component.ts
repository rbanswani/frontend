import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ServicesModule } from 'src/app/services/services.module';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
    // signform: FormGroup;

    registerForm: FormGroup;
    submitted = false;

    constructor(private router:Router,private formBuilder: FormBuilder, private sgn: ServicesModule) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(4)]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onsign() {
        this.submitted = true;
        console.log(this.registerForm.value);
        this.sgn.sign(this.registerForm.value)
            .subscribe((res: any) => {
                res = res.data;
                console.log("data", res)
            })
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
            else{
                // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
                this.router.navigate(['/company/login']);
            }
       
    }
}





































//   constructor(private sgu: ServicesModule) { }

//   firstName: string;
//   email: string;
//   password: string;

//   ngOnInit() {
//     // this.signform= new FormGroup({
//     //   'firstName' :new FormControl(''),
//     //   'email':new FormControl(''),
//     //   'password':new FormControl('')},
//     // );

//   }
//   submit() {
//     console.log(this.firstName,"this.firstName");
//     var obj={
//       firstName:this.firstName,
//       email:this.email,
//       password:this.password
//     }
//       this.sgu.sign(obj)
//      .subscribe((res:any)=>{
//       res=res.data;
//       console.log("data",res)
//     })
//   }

// }

