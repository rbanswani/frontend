import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AuthRoutingModule } from './/auth-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServicesModule } from '../../areas/global-pages/services/services.module';
import { SignupComponent } from 'src/app/library/auth/signup/signup.component';
import { BlogsComponent } from './blogs/blogs.component';
import { ReadblogComponent } from './readblog/readblog.component';
import { FullblogComponent } from './fullblog/fullblog.component';
import {RatingModule} from 'primeng/rating';

@NgModule({
  imports: [
    CommonModule,
    
    AuthRoutingModule,
    FormsModule,
    ServicesModule,
    RatingModule,
    ReactiveFormsModule,
  

    
  ],
  declarations: [LoginComponent, ForgotPasswordComponent,SignupComponent, BlogsComponent, ReadblogComponent, FullblogComponent ],
  bootstrap: [SignupComponent]
})
export class AuthModule { }
