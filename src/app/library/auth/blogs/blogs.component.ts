import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BloggerService } from '../../../services/blogger.service';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
  listArr:any
  
  constructor(private router: Router, private bro: BloggerService,private route:ActivatedRoute) { }

  ngOnInit() {
    this.getlistblog();
  //   var myIndex = 0;
  //    carousel();
  //   function carousel() {
  //     var i;
  //     var x = document.getElementsByClassName("mySlides");
  //     for (i = 0; i < x.length; i++) {
  //        x[i].style.display = "none";  
  //     }
  //     myIndex++;
  //     if (myIndex > x.length) {myIndex = 1}    
  //     x[myIndex-1].style.display = "block";  
  //     setTimeout(carousel, 4000);    
  // }



  
 }

  getlistblog() {
    this.route.params.subscribe(params => {
       this.bro.listBlog(params['id']).subscribe(res => {
 this.listArr = res;
  console.log("blog list show", this.listArr)

      })
     })
   }
  };
