import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommentService } from '../../../services/comment.service';
@Component({
  selector: 'app-fullblog',
  templateUrl: './fullblog.component.html',
  styleUrls: ['./fullblog.component.css']
})
export class FullblogComponent implements OnInit {
  val: Number
  userid: string;
  commentList: any;
  userList = new Array;
  listBlog = new Array;
  constructor(private router: Router,
    private route: ActivatedRoute,
    private add: CommentService
  ) { }

  ngOnInit() {
    this.getComments();


  }

  getComments() {
    this.route.params.subscribe(params => {
      console.log((params['id']));
      this.add.getComments(params['id']).subscribe((res: any) => {

        this.commentList = res;

        console.log("blog list show", this.commentList);
        for (var i = 0; i < this.commentList.length; i++) {
          this.userList.push(this.commentList[i].userid);
          this.commentList[i].firstName = this.userList[i].firstName;
        }
        this.listBlog.push(this.commentList[0].blogid);

        console.log("===========name", this.commentList)
        console.log("=========name", this.listBlog)
      })
    })
  }
  addCom() {

    console.log(this.val);
    var comment = (document.getElementById('comment') as HTMLInputElement).value;
    this.route.params.subscribe(params => {
      var data = {
        userid: localStorage.getItem("_id"),
        blogid: params.id,
        comment: comment,
        ratings: this.val,
        date: new Date()
      }
      this.add.addcomment(data).subscribe((res: any) => {
        res = res.data;
        console.log("Data is here", res);
      }
      )
    }
    )
  }

}

