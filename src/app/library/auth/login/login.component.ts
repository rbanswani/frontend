import { Component, OnInit } from '@angular/core';
import { ServicesModule } from 'src/app/services/services.module';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
    private myRoute: Router, private auth: AuthService, 
    private lgn: ServicesModule,
     private router: Router,
     private toastr: ToastrService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({

      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onlogin() {
    this.submitted = true;
    console.log("!<><<<<,", this.registerForm.value);
    this.lgn.login(this.registerForm.value).subscribe((res: any) => {
      if (res.token) {
        // this.router.navigate(["company/dashboard"])

        // res=res.data;
        console.log("response >>>>>> : :", res);
        localStorage.setItem("token", res.token);
        
        console.log("data", res, 'role', localStorage.getItem("role"), localStorage.getItem("token"))
        var data = res.data;
        localStorage.setItem("role",data.role);
        localStorage.setItem("_id",data._id);
        if (localStorage.getItem("token") != null && localStorage.getItem("role") == "admin") {
          this.toastr.success('Hello Admin!', 'Logged in successfully!');
          this.router.navigate(["/user/dashboard"]);
        }
        else {
          this.router.navigate(["/userboard"])
          this.toastr.success('Hello User!', 'Logged in successfully!');
        };

        console.log("data", res)
      }
      else {
        console.log("invalid user name and password")
        // this.router.navigate(["userboard"])
      }
    })
  }
  login() {
    if (this.registerForm.valid) {
      this.auth.sendToken(this.registerForm.value.email)
      this.myRoute.navigate(["company/dashboard"]);
      alert("successss");
      
    }
  }



}
















































//   constructor(private sgu: ServicesModule,private router: Router) { }


//   email: string;
//   password: string;

//   ngOnInit() {

//   }
//   login() {
//     console.log(this.email, "this.email");
//     var obj = {
//       email: this.email,
//       password: this.password
//     }


//     this.sgu.login(obj)
//       .subscribe((res: any) => {
//         console.log('response   : :', res);
//         if (res.token) {
//           console.log("valid user")

//           //  this.router.navigate(["dashboard"])
//         }
//         else {
//           console.log("invalid user name and password")
//         }
//         // res=res.data;
//         console.log("response >>>>>> : :", res);
//         localStorage.setItem("token", res.token);
//         // console.log("data", res,'role',localStorage.getItem("role"),localStorage.getItem("token"))
//         // var data = res.data;
//         localStorage.setItem("role", res.data.role);

//         if (localStorage.getItem("token") != null && localStorage.getItem("role") == "admin") {
//           this.router.navigate(["/admin/dashboard"]);
//         }
//         else {
//           this.router.navigate(["/user/userboard"])
//         };


//       })
//   }
// }



