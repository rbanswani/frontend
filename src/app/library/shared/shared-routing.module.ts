import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';


const routes: Routes=[
  {path:'home',component:HomeComponent},
  {path:'',redirectTo:'home',pathMatch:'full'}
  
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
exports:[RouterModule],



  declarations: []
})
export class SharedRoutingModule { }
