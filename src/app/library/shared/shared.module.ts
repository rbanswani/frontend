import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './/shared-routing.module';
// import { HomeComponent } from './components/home/home.component';
// import { GlobalHeaderComponent } from './components/global-header/global-header.component';
// import { GlobalFooterComponent } from './components/global-footer/global-footer.component';




@NgModule({
  imports: [
    CommonModule,
    SharedRoutingModule,
    // GlobalHeaderComponent,
    // GlobalFooterComponent,

  ],
  // declarations: [HomeComponent, GlobalHeaderComponent,GlobalFooterComponent ]
})
export class SharedModule { }
