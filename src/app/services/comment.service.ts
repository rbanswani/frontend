import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CommentService {


  constructor(private http: HttpClient) { }

  getComments(id: any): Observable<any> {
    console.log("check the comments")
    return this.http.get("http://localhost:10011/getComments?id=" + id);
  }


  addcomment(data: any): Observable<any> {
    console.log("comments",data)
    return this.http.post("http://localhost:10011/addcomment", data);
  }


}
