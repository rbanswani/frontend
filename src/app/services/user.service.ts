import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }



  listUser():Observable<any>{
      console.log("check list")
      return this.http.get("http://localhost:10011/listUser");}


    deleteUsers(_id):Observable<any>{
      console.log("check delete")
      return this.http.delete("http://localhost:10011/deleteUsers/"+_id);
    }
  }


