import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class ProductService {
 

 
  constructor(private http:HttpClient) { }


  addproduct(data:any):Observable<any>{
  console.log("data#3443242",data)
   return this.http.post("http://localhost:10011/addproduct",data);}


  listproduct():Observable<any>{
    console.log("check product list")
    return this.http.get("http://localhost:10011/listproduct");}

    
    deleteProduct(_id):Observable<any>{
      console.log("check delete")
      return this.http.delete("http://localhost:10011/deleteProduct/"+_id);
    }
   
  Editproduct(data,id):Observable<any>{
    console.log("check edit")
    return this.http.put("http://localhost:10011/editProduct?=id"+id,data);
   
  }
  }

