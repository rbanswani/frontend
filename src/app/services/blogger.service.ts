import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BloggerService {
  confirm(arg0: any): any {
    throw new Error("Method not implemented.");
  }

  constructor(private http:HttpClient) { }

addBlog(data:any):Observable<any>{
  console.log("data#3443242",data)
  return this.http.post("http://localhost:10011/addBlog",data);}


listBlog(id:any):Observable<any>{
  console.log("check blogg list")
  return this.http.get("http://localhost:10011/listBlog?id="+id);}

  deleteBlog(_id):Observable<any>{
    console.log("check delete")
    return this.http.delete("http://localhost:10011/deleteBlog/"+_id);
  }

  EditBlog(data,id):Observable<any>{
    console.log("check edit")
    return this.http.put("http://localhost:10011/editBlog?=id"+id,data);
   
  }
 
}