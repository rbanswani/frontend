import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './library/shared/components/home/home.component';
import { GlobalHeaderComponent } from './library/shared/components/global-header/global-header.component';
import { GlobalFooterComponent } from './library/shared/components/global-footer/global-footer.component';
import { ReactiveFormsModule, FormsModule, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './auth.guard';
import { CommonModule } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { ToastrModule } from 'ngx-toastr'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
   
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,

    GlobalHeaderComponent,
    GlobalFooterComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),


  ],
  providers: [AuthService,
    AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
